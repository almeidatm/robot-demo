*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${url_address}    https://cloudnapratica.com/
${message}    //h1[contains(normalize-space(),'AWS')]

*** Test Cases ***
Should display message in home page

    Open Browser  ${url_address}  headlesschrome
    Page should contain element  ${message}
    Close All Browsers
